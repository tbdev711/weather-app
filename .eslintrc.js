module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true
    },
    extends: ['plugin:react/recommended', 'plugin:prettier/recommended', 'plugin:import/errors'],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
        it: 'readonly',
        expect: 'readonly',
        describe: 'readonly',
        jest: 'readonly',
        beforeEach: 'readonly'
    },
    parserOptions: {
        ecmaFeatures: {
            jsx: true
        },
        ecmaVersion: 2018,
        sourceType: 'module'
    },
    plugins: ['react', 'react-hooks', 'prettier', 'import'],
    settings: {
        react: {
            version: 'detect'
        }
    },
    rules: {
        'prettier/prettier': 'error'
    }
};
