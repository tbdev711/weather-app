# Weather App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app),
and is powered by web APIs, React, Redux, and Google Cloud.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs required dependencies

### `npm run start:backend`

Starts the backend server on Port 3001

### `npm run start:webpack`

Starts the hot-reloading development server on Port 3000

The development server is automatically configured to use the backend server as its reverse proxy

### `npm run test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run test:unit`

Launches the test runner in CI mode and generates coverage reports.<br />

## Secrets

A .env file is required to standup the application. Please contact a [contributor](https://gitlab.com/tbdev711/weather-app/-/graphs/dev) for the contents.

## Deployed Version

View the deployed version [here](https://weather-app-qxdh4e7qna-uc.a.run.app/)
