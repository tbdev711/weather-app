import React from 'react';
import { Weather } from './containers';
import { store } from './store';
import { Provider } from 'react-redux';

function App() {
    return (
        <Provider store={store}>
            <Weather />
        </Provider>
    );
}

export default App;
