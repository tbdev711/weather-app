import React from 'react';
import { render, fireEvent, waitForElement } from '@testing-library/react';

import { Home } from './Home';

describe('The Home Page', () => {
    const defaultProps = {
        weather: {
            currently: {},
            minutely: {},
            daily: {
                data: []
            },
            hourly: {
                data: []
            },
            alerts: []
        },
        geolocation: { lat: 42, lng: 42 },
        onCitySelect: jest.fn(),
        cityName: '',
        cityState: ''
    };

    function renderHome(overrideProps = {}) {
        const props = {
            ...defaultProps,
            ...overrideProps
        };

        return render(<Home {...props} />);
    }

    it('should not show details by default', () => {
        // arrange
        // act
        const { queryByTestId } = renderHome();

        // assert
        expect(queryByTestId('details-section')).not.toBeInTheDocument();
    });

    it('should not show show more button if current weather is unavailable', () => {
        // arrange
        const propsUnderTest = {
            weather: {}
        };
        // act
        const { queryByTestId } = renderHome(propsUnderTest);

        // assert
        expect(queryByTestId('show-details-button')).not.toBeInTheDocument();
    });

    it('should show show more button if current weather is available', () => {
        // arrange
        // act
        const { queryByTestId } = renderHome();

        // assert
        expect(queryByTestId('show-details-button')).toBeInTheDocument();
    });

    it('should show details when the open button is clicked', async () => {
        // arrange
        // act
        const { queryByTestId, getByTestId } = renderHome();
        fireEvent.click(getByTestId('show-details-button'));
        await waitForElement(() => getByTestId('details-section'));

        // assert
        expect(queryByTestId('details-section')).toBeInTheDocument();
    });

    it('should not show details when the hide button is clicked', async () => {
        // arrange
        // act
        const { queryByTestId, getByTestId } = renderHome();
        fireEvent.click(getByTestId('show-details-button'));
        await waitForElement(() => getByTestId('details-section'));
        fireEvent.click(getByTestId('hide-details-button'));
        await waitForElement(() => getByTestId('show-details-button'));

        // assert
        expect(queryByTestId('details-section')).not.toBeInTheDocument();
    });

    it('should scroll to the details section after the show more button is clicked', async () => {
        // arrange
        const mockFn = jest.fn();
        window.HTMLElement.prototype.scrollIntoView = mockFn;

        // act
        const { getByTestId } = renderHome();
        fireEvent.click(getByTestId('show-details-button'));
        await waitForElement(() => getByTestId('details-section'));

        // assert
        expect(mockFn).toHaveBeenCalledTimes(2);
    });

    it('should scroll to the current section when the weather is fetched', async () => {
        // arrange
        const mockFn = jest.fn();
        window.HTMLElement.prototype.scrollIntoView = mockFn;

        // act
        const { getByTestId } = renderHome();
        await waitForElement(() => getByTestId('show-details-button'));

        // assert
        expect(mockFn).toHaveBeenCalled();
    });
});
