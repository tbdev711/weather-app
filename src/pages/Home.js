import React, { createRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Search, Overview, DetailedSummary } from '../components';
import { faArrowCircleUp, faArrowCircleDown } from '@fortawesome/free-solid-svg-icons';
import config from 'react-reveal/globals';

config({ ssrFadeout: true });

export function Home({ weather, geolocation, onCitySelect, cityName, cityState }) {
    const currentRef = createRef();
    const detailsRef = createRef();
    const [showDetails, setShowDetails] = useState(false);

    useEffect(() => {
        if (showDetails && detailsRef.current) {
            detailsRef.current.scrollIntoView({ behavior: 'smooth' });
        }
    }, [showDetails, detailsRef]);

    useEffect(() => {
        if (weather.currently && currentRef.current && !showDetails) {
            currentRef.current.scrollIntoView({ behavior: 'smooth' });
        }
    }, [weather, geolocation, currentRef, showDetails]);

    function handleOnCitySelect(city) {
        setShowDetails(false);
        onCitySelect(city);
    }

    return (
        <>
            <section className="landing">
                <Search onCitySelect={handleOnCitySelect} />
            </section>
            <section ref={currentRef} className="has-background-dark">
                <br />
                <Overview
                    current={weather.currently}
                    minutely={weather.minutely}
                    daily={weather.daily}
                    hourly={weather.hourly}
                    alerts={weather.alerts}
                    city={cityName}
                    cityState={cityState}
                    lat={geolocation.lat}
                    lng={geolocation.lng}
                />
            </section>
            <div ref={detailsRef}>
                {showDetails ? (
                    <>
                        <section data-testid="details-section" className="has-background-light">
                            <DetailedSummary
                                lat={geolocation.lat}
                                lng={geolocation.lng}
                                daily={weather.daily}
                                hourly={weather.hourly}
                            />
                        </section>

                        <div
                            data-testid="hide-details-button"
                            className="has-text-centered has-text-primary has-pointer hoverEffect"
                            onClick={() => setShowDetails(false)}
                        >
                            <br />
                            <div>
                                <FontAwesomeIcon icon={faArrowCircleUp} size="2x" />
                                <p>Back </p>
                            </div>
                        </div>
                    </>
                ) : weather.currently ? (
                    <div style={{ height: 90 }}>
                        <div
                            data-testid="show-details-button"
                            className="has-text-centered has-text-primary has-pointer hoverEffect"
                            onClick={() => setShowDetails(true)}
                        >
                            <p>Detailed Forecast </p> <FontAwesomeIcon icon={faArrowCircleDown} size="2x" />
                        </div>
                    </div>
                ) : null}
            </div>
        </>
    );
}

Home.propTypes = {
    weather: PropTypes.object.isRequired,
    geolocation: PropTypes.object.isRequired,
    onCitySelect: PropTypes.func.isRequired,
    cityName: PropTypes.string,
    cityState: PropTypes.string
};
