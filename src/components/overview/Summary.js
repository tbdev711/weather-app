import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { getIcon, getWindDirection } from '../../utils';
import { CurrentSlide } from '../slider/slides';
import moment from 'moment';

export function Summary({
    city,
    summary,
    icon,
    temperature,
    apparentTemperature,
    precipProbability,
    windGust,
    windBearing,
    time,
    humidity,
    cloudCover,
    dewPoint,
    uvIndex,
    visibility,
    pressure
}) {
    return (
        <>
            <p className="box title is-uppercase has-background-primary has-text-white">{city}</p>
            <p className="title is-4 has-text-white">{moment.unix(time).format('h:mm A')}</p>
            <p className="title is-4 has-text-white">{summary}</p>
            <img src={getIcon(icon)}></img>
            <br />
            <br />
            <p className="title is-3 has-text-white "> {Math.round(temperature)}°F </p>
            <p className="title is-5 has-text-white ">Feels like: {Math.round(apparentTemperature)}°F </p>
            <p>Chance of Precip: {precipProbability}%</p>
            <p>
                Wind Gusts: {Math.round(windGust)}mph {' from '} {getWindDirection(windBearing)}
            </p>
            <CurrentSlide
                humidity={Math.floor(humidity * 100)}
                cloudCover={Math.floor(cloudCover * 100)}
                dewPoint={Math.round(dewPoint)}
                uvIndex={uvIndex}
                visibility={Math.round(visibility)}
                pressure={Math.round(pressure)}
                time={time}
            />
        </>
    );
}

Summary.propTypes = {
    city: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    temperature: PropTypes.number.isRequired,
    apparentTemperature: PropTypes.number.isRequired,
    precipProbability: PropTypes.number.isRequired,
    windGust: PropTypes.number.isRequired,
    windBearing: PropTypes.number.isRequired,
    humidity: PropTypes.number.isRequired,
    cloudCover: PropTypes.number.isRequired,
    dewPoint: PropTypes.number.isRequired,
    uvIndex: PropTypes.number.isRequired,
    visibility: PropTypes.number.isRequired,
    pressure: PropTypes.number.isRequired,
    time: PropTypes.number.isRequired
};
