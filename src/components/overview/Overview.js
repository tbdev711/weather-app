import React from 'react';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { CurrentPropTypes } from '../../prop-types';
import { Summary } from './Summary';
import { OutlookSlide } from '../slider/slides';
import Alert from '../Alert';

export function Overview({ current, minutely = {}, daily, alerts = [], city }) {
    return (
        <>
            <section>
                <div className="box has-background-light " style={{ height: '75%' }}>
                    <div className="tile is-ancestor has-text-centered" style={{ height: '75%' }}>
                        {current ? (
                            <>
                                <div
                                    className="tile is-6 is-vertical is-parent box has-background-light has-text-white"
                                    style={{ height: '75%' }}
                                >
                                    <Fade left>
                                        <div
                                            className="tile is-child"
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                flexDirection: 'column'
                                            }}
                                        >
                                            <Summary city={city} {...current} />
                                        </div>
                                    </Fade>
                                </div>

                                <div className="tile is-child">
                                    <Fade left>
                                        <OutlookSlide daily={daily.data} />
                                        <br />
                                        {alerts.length < 1 ? (
                                            <div className="box has-background-primary has-text-white title is-5">
                                                <FontAwesomeIcon icon={faInfoCircle} size="1x" />
                                                <span>There are no active alerts in your area.</span>
                                            </div>
                                        ) : (
                                            alerts.map((alert) => <Alert key={alert.title} {...alert} />)
                                        )}
                                    </Fade>
                                </div>
                            </>
                        ) : (
                            <div className="has-text-centered">
                                <FontAwesomeIcon icon={faSpinner} spin size="4x" className="has-text-white" />
                            </div>
                        )}
                    </div>
                </div>
            </section>
        </>
    );
}

Overview.propTypes = {
    alerts: PropTypes.array,
    city: PropTypes.string.isRequired,
    current: PropTypes.shape(CurrentPropTypes),
    daily: PropTypes.shape({
        data: PropTypes.array.isRequired
    }),
    minutely: PropTypes.object
};
