import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

function AlertModal({ onClose, className, title, time, regions = [], description, expires }) {
    return (
        <div key="modal">
            <div key={title} id="modal" className="modal has-background-primary has-text-white title is-5 is-active">
                <div className="modal-background">
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <span className={className}>
                                <p className="modal-card-title has-text-centered has-text-white">{title}</p>
                                <FontAwesomeIcon icon={faInfoCircle} className="has-text-white" />
                            </span>
                            <button onClick={onClose} className="modal-close is-large" aria-label="close"></button>
                        </header>

                        <section className="modal-card-body has-text-info">
                            <p className="title is-5 has-text-info">
                                Issued at {moment.unix(time).format('h:mm A on ddd MMMM Do ')} for:
                            </p>
                            <p className="title is-5">{regions.join(', ')}</p>
                            <br />
                            <p className="title is-5 has-text-justified">{description}</p>
                            <span>More Details - uri </span>
                            <br />
                            <br />
                            <span>Expires: {moment.unix(expires).format('ddd MMMM Do h:mm A')}</span>
                            <br />
                            <br />
                        </section>
                    </div>
                </div>
            </div>
        </div>
    );
}
AlertModal.propTypes = {
    title: PropTypes.string.isRequired,
    severity: PropTypes.string.isRequired,
    time: PropTypes.number.isRequired,
    regions: PropTypes.array,
    description: PropTypes.string.isRequired,
    uri: PropTypes.string.isRequired,
    expires: PropTypes.number.isRequired,
    onClose: PropTypes.func.isRequired,
    color: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired
};
export default AlertModal;
