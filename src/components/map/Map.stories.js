import React from 'react';

import { Map } from '.';

export default {
    title: 'Map',
    component: Map
};

export const DefaultMap = () => <Map lat={84} lng={42} />;
