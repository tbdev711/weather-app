import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
import GoogleMapReact from 'google-map-react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons';

let timeout;
export function Map({ lat, lng }) {
    const zoom = 7;

    const [timestamps, setTimestamps] = useState([]);
    const [index, setIndex] = useState(0);
    const [gMaps, setGMaps] = useState({ api: null, instance: null });
    const [shouldAnimate, setShouldAnimate] = useState(false);

    async function fetchTimestamps() {
        const { data } = await axios.get('https://api.rainviewer.com/public/maps.json');
        setTimestamps(data);
    }

    useEffect(() => {
        fetchTimestamps();
    }, []);

    useEffect(() => {
        if (shouldAnimate && gMaps.instance) {
            timeout = setTimeout(() => {
                let idx = index;
                gMaps.instance.overlayMapTypes.getAt(idx).setOpacity(0.0);

                idx++;
                if (idx > gMaps.instance.overlayMapTypes.getLength() - 1) {
                    idx = 0;
                }
                gMaps.instance.overlayMapTypes.getAt(idx).setOpacity(0.6);
                setIndex(idx);
            }, 400);
        } else if (gMaps.instance && gMaps.instance.overlayMapTypes.getAt(index)) {
            timestamps.forEach((_, idx) => {
                gMaps.instance.overlayMapTypes.getAt(idx).setOpacity(0.0);

                if (idx === index) {
                    gMaps.instance.overlayMapTypes.getAt(idx).setOpacity(0.6);
                }
            });
        }
    }, [gMaps, shouldAnimate, index, timestamps]);

    function toggleAnimation() {
        clearTimeout(timeout);
        setShouldAnimate(!shouldAnimate);
    }

    return (
        <div style={{ height: '80vh', width: '100%', position: 'relative' }}>
            {timestamps.length > 0 && (
                <>
                    <div className="buttons is-centered">
                        {timestamps.map((timestamp, idx) => (
                            <button
                                onClick={() => {
                                    clearTimeout(timeout);
                                    setShouldAnimate(false);
                                    setIndex(idx);
                                }}
                                key={timestamp}
                                className={classnames('button is-medium is-light', {
                                    'has-background-primary': index === idx
                                })}
                            >
                                {moment.unix(timestamp).format('h:mm A')}
                            </button>
                        ))}
                        <button
                            className="button is-medium has-background-primary has-text-white"
                            onClick={toggleAnimation}
                        >
                            {shouldAnimate ? <FontAwesomeIcon icon={faPause} /> : <FontAwesomeIcon icon={faPlay} />}
                        </button>
                    </div>
                    <div style={{ height: '90%' }}>
                        <GoogleMapReact
                            bootstrapURLKeys={{ key: 'AIzaSyBololZt5uhLhteTmV5XH5vZzXZI77D_yM' }}
                            onGoogleApiLoaded={({ map, maps }) => {
                                timestamps.forEach((timestamp, idx) => {
                                    const imageMapType = new maps.ImageMapType({
                                        getTileUrl: function (coord, zoom) {
                                            return `https://tilecache.rainviewer.com/v2/radar/${timestamp}/256/${zoom}/${coord.x}/${coord.y}/4/1_1.png`;
                                        },
                                        tileSize: new maps.Size(256, 256),
                                        opacity: idx === index ? 0.6 : 0.0
                                    });

                                    map.overlayMapTypes.push(imageMapType);
                                });
                                setGMaps({ api: maps, instance: map });
                            }}
                            center={{ lat, lng }}
                            zoom={zoom}
                        ></GoogleMapReact>
                    </div>
                </>
            )}
        </div>
    );
}

Map.propTypes = {
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired
};
