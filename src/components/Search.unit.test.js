import React from 'react';
import { render, waitForElement, fireEvent } from '@testing-library/react';
import axios from 'axios';
import { debounce } from 'lodash';

import { Search } from './Search';

jest.mock('lodash');
jest.mock('axios');

describe('The Search Component', () => {
    beforeAll(() => {
        debounce.mockImplementation((callback) => {
            return callback;
        });
    });

    it('should show cities when there are cities and the search is focused', async () => {
        // arrange
        const onCitySelect = jest.fn();
        axios.get.mockResolvedValue({ data: [{ formatted_address: 'address' }] });

        // act
        const { queryByTestId, getByPlaceholderText, getByTestId } = render(<Search onCitySelect={onCitySelect} />);
        await waitForElement(() => getByPlaceholderText('Search location'));
        fireEvent.focus(getByPlaceholderText('Search location'));
        fireEvent.change(getByPlaceholderText('Search location'), { target: { value: 'value' } });
        await waitForElement(() => getByTestId('cities-dropdown'));

        // assert
        expect(queryByTestId('cities-dropdown')).toBeVisible();
    });

    it('should not show cities when there are no cities and the search is focused', async () => {
        // arrange
        const onCitySelect = jest.fn();
        axios.get.mockResolvedValue({ data: [] });

        // act
        const { queryByTestId, getByPlaceholderText, getByTestId } = render(<Search onCitySelect={onCitySelect} />);
        await waitForElement(() => getByPlaceholderText('Search location'));
        fireEvent.focus(getByPlaceholderText('Search location'));
        fireEvent.change(getByPlaceholderText('Search location'), { target: { value: 'value' } });
        await waitForElement(() => getByTestId('cities-dropdown'));

        // assert
        expect(queryByTestId('cities-dropdown')).not.toBeVisible();
    });

    it('should not show cities after a city has been selected', async () => {
        // arrange
        const onCitySelect = jest.fn();
        axios.get.mockResolvedValue({ data: [{ formatted_address: 'address' }] });

        // act
        const { queryByTestId, getByPlaceholderText, getByTestId, getByText } = render(
            <Search onCitySelect={onCitySelect} />
        );
        await waitForElement(() => getByPlaceholderText('Search location'));
        fireEvent.focus(getByPlaceholderText('Search location'));
        fireEvent.change(getByPlaceholderText('Search location'), { target: { value: 'value' } });
        await waitForElement(() => getByText('address'));
        fireEvent.click(getByText('address'));
        await waitForElement(() => getByTestId('cities-dropdown'));

        // assert
        expect(queryByTestId('cities-dropdown')).not.toBeVisible();
    });

    it('should call onCitySelect with the city after a city has been selected', async () => {
        // arrange
        const onCitySelect = jest.fn();
        axios.get.mockResolvedValue({ data: [{ formatted_address: 'address' }] });

        // act
        const { getByPlaceholderText, getByTestId, getByText } = render(<Search onCitySelect={onCitySelect} />);
        await waitForElement(() => getByPlaceholderText('Search location'));
        fireEvent.focus(getByPlaceholderText('Search location'));
        fireEvent.change(getByPlaceholderText('Search location'), { target: { value: 'value' } });
        await waitForElement(() => getByText('address'));
        fireEvent.click(getByText('address'));
        await waitForElement(() => getByTestId('cities-dropdown'));

        // assert
        expect(onCitySelect).toHaveBeenCalledWith({ formatted_address: 'address' });
    });

    it('should not show cities when there are cities and something other than the search is clicked', async () => {
        // arrange
        const onCitySelect = jest.fn();
        axios.get.mockResolvedValue({ data: [{ formatted_address: 'address' }] });

        // act
        const { queryByTestId, getByPlaceholderText, getByTestId, getByText } = render(
            <Search onCitySelect={onCitySelect} />
        );
        await waitForElement(() => getByPlaceholderText('Search location'));
        fireEvent.focus(getByPlaceholderText('Search location'));
        fireEvent.change(getByPlaceholderText('Search location'), { target: { value: 'value' } });
        fireEvent.mouseUp(getByText('TB-Dev Weather'));
        await waitForElement(() => getByTestId('cities-dropdown'));

        // assert
        expect(queryByTestId('cities-dropdown')).not.toBeVisible();
    });
});
