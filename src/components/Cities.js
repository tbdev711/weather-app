import React from 'react';
import PropTypes from 'prop-types';

export function Cities({ shouldShowCities, cities, onCitySelect }) {
    const display = shouldShowCities && cities.length > 0 ? 'block' : 'none';
    return (
        <div style={{ display }} data-testid="cities-dropdown" className="dropdown-menu" id="dropdown-menu" role="menu">
            <div className="dropdown-content">
                {cities.map((city) => {
                    return (
                        <span onClick={() => onCitySelect(city)} key={city.formatted_address} className="dropdown-item">
                            {city.formatted_address}
                        </span>
                    );
                })}
            </div>
        </div>
    );
}

Cities.propTypes = {
    cities: PropTypes.array.isRequired,
    onCitySelect: PropTypes.func.isRequired,
    shouldShowCities: PropTypes.bool.isRequired
};
