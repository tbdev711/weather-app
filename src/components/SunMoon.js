import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { getMoonPhase } from '../utils';
import { ReactComponent as SunriseIcon } from '../svg/Sunrise.svg';
import { ReactComponent as SunsetIcon } from '../svg/Sunset.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleUp, faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons';

export function SunMoon(props) {
    const { Icon, label } = getMoonPhase(props.moonPhase);

    return (
        <div className="tile is-ancester">
            <div className="tile is-child box has-background-light">
                <br />

                <div className="image is-fullwidth is-256x256">
                    <FontAwesomeIcon
                        icon={faAngleDoubleUp}
                        className="has-text-success"
                        size="5x"
                        style={{ paddingLeft: '11px' }}
                    />
                    <SunriseIcon />
                </div>
                <br></br>
                <p className=" title is-4 has-text-white">
                    {moment.unix(props.sunriseTime).format(props.datetimeFormat)}
                </p>
                <p className="subtitle is-6 has-text-white">SUNRISE</p>
            </div>
            g
            <div className="tile is-child box has-background-light">
                <br />
                <p className=" title is-4 has-text-white">
                    {moment.unix(props.sunsetTime).format(props.datetimeFormat)}
                </p>
                <p className="subtitle is-6 has-text-white">SUNSET</p>
                <div className="image is-fullwidth">
                    <SunsetIcon />
                    <FontAwesomeIcon
                        icon={faAngleDoubleDown}
                        className="has-text-success"
                        size="5x"
                        style={{ paddingRight: '7px' }}
                    />
                </div>
            </div>
            <div className="tile is-child box has-background-light">
                <br />
                <p className=" title is-4 has-text-white"> {label} </p>
                <p className="subtitle is-6 has-text-white">MOON PHASE</p>
                <div className="image is-fullwidth">
                    <Icon />
                </div>
            </div>
        </div>
    );
}

SunMoon.propTypes = {
    sunsetTime: PropTypes.number.isRequired,
    sunriseTime: PropTypes.number.isRequired,
    moonPhase: PropTypes.number.isRequired,
    datetimeFormat: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired
};
