import React from 'react';

import { CurrentPropTypes } from '../../../prop-types';

export function CurrentSlide(props) {
    return (
        <>
            <br></br>
            <div className="columns">
                <div className="column">
                    <div className="box title is-5 has-background-primary has-text-white">
                        Humidity: {props.humidity}%
                    </div>
                    <div className="box title is-5 has-background-primary has-text-white">
                        Cloud Cover: {props.cloudCover}%
                    </div>
                    <div className="box title is-5 has-background-primary has-text-white">
                        Dewpoint: {props.dewPoint}°F
                    </div>
                </div>

                <div className="column">
                    <div className="box title is-5 has-background-primary has-text-white">
                        UV Index: {props.uvIndex}
                    </div>
                    <div className="box title is-5 has-background-primary has-text-white">
                        Visibility: {props.visibility}mi
                    </div>
                    <div className="box title is-5 has-background-primary has-text-white">
                        Pressure: {props.pressure}mb
                    </div>
                </div>
            </div>
        </>
    );
}

CurrentSlide.propTypes = {
    ...CurrentPropTypes
};
