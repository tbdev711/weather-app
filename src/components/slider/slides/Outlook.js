import React from 'react';
import PropTypes from 'prop-types';
import Fade from 'react-reveal/Fade';

import { Daily } from '../../Daily';

export function OutlookSlide(props) {
    return (
        <>
            <br></br>
            <br />
            <div className="has-text-white title is-5 has-background-primary is-uppercase">Outlook</div>

            <div>
                <Fade right cascade>
                    <div className="tile">
                        {props.daily
                            .slice(0, 3)
                            .map(({ time, icon, apparentTemperatureHigh, apparentTemperatureLow, summary }) => (
                                <Daily
                                    key={time}
                                    time={time}
                                    dateFormat="ddd"
                                    icon={icon}
                                    apparentTemperatureHigh={apparentTemperatureHigh}
                                    apparentTemperatureLow={apparentTemperatureLow}
                                    summary={summary}
                                />
                            ))}
                    </div>
                </Fade>
            </div>
        </>
    );
}

OutlookSlide.propTypes = {
    daily: PropTypes.array.isRequired
};
