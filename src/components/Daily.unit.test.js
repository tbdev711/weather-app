import React from 'react';
import { render } from '@testing-library/react';
import moment from 'moment';
import { Daily } from './Daily';

describe('The Daily Component', () => {
    const defaultProps = {
        summary: 'test summary',
        apparentTemperatureHigh: 0,
        apparentTemperatureLow: 0,
        dateFormat: 'ddd',
        icon: 'clear-day',
        time: 0
    };

    it('should show the summary', () => {
        // arrange
        const props = {
            ...defaultProps,
            summary: 'test summary'
        };

        // act
        const { queryByText } = render(<Daily {...props} />);

        // assert
        expect(queryByText(props.summary)).toBeInTheDocument();
    });

    it('should show the high apparent temperature', () => {
        // arrange
        const props = {
            ...defaultProps,
            apparentTemperatureHigh: 88
        };

        // act
        const { queryByText } = render(<Daily {...props} />);

        // assert
        expect(queryByText(`High: ${props.apparentTemperatureHigh}`)).toBeInTheDocument();
    });

    it('should show the low apparent temperature', () => {
        // arrange
        const props = {
            ...defaultProps,
            apparentTemperatureLow: -32
        };

        // act
        const { queryByText } = render(<Daily {...props} />);

        // assert
        expect(queryByText(`Low: ${props.apparentTemperatureLow}`)).toBeInTheDocument();
    });

    it('should show the day', () => {
        // arrange
        const props = {
            ...defaultProps,
            dateFormat: 'ddd',
            time: moment().unix()
        };

        // act
        const { queryByText } = render(<Daily {...props} />);

        // assert
        expect(queryByText(moment.unix(props.time).format(props.dateFormat))).toBeInTheDocument();
    });
});
