import React from 'react';
import { render } from '@testing-library/react';

import Alert from './Alert';

describe('The Alerts Component', () => {
    it('should show alert', () => {
        // arrange
        const alert = {
            title: 'title',
            severity: 'severity',
            time: 123,
            regions: ['regions'],
            description: 'description',
            uri: 'uri',
            expires: 0
        };
        // act
        const { queryByText } = render(<Alert {...alert} />);

        // assert
        expect(queryByText('title')).toBeInTheDocument();
    });
});
