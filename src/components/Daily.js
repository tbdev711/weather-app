import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { getIcon } from '../utils';

export function Daily(props) {
    return (
        <div className="tile is-child box has-background-light">
            <p className="title is-6 has-text-white">{moment.unix(props.time).format(props.dateFormat)}</p>
            <img src={getIcon(props.icon)}></img>
            <br />
            <p className="title is-6 has-text-white">High: {Math.round(props.apparentTemperatureHigh)}</p>
            <p className="title is-6 has-text-white">Low: {Math.round(props.apparentTemperatureLow)}</p>
            <p className="title is-6 has-text-white">{props.summary}</p>
        </div>
    );
}

Daily.propTypes = {
    apparentTemperatureHigh: PropTypes.number.isRequired,
    apparentTemperatureLow: PropTypes.number.isRequired,
    dateFormat: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    time: PropTypes.number.isRequired
};
