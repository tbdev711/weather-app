import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import AlertModal from './Alert-Modal';

function Alert({ title, severity, ...alert }) {
    const [showAlert, setShowAlert] = useState(false);
    const className = classnames('has-text-white button is-medium is-fullwidth', {
        'has-background-primary': severity === 'advisory',
        'has-background-success': severity === 'watch',
        'has-background-danger': severity === 'warning'
    });

    return (
        <div>
            <br />
            {showAlert && (
                <AlertModal onClose={() => setShowAlert(false)} className={className} title={title} {...alert} />
            )}
            <button key="modal" id="modal" onClick={() => setShowAlert(true)} className={className}>
                <span className="title is-4 has-text-white">
                    <span>{title}</span>
                    <FontAwesomeIcon icon={faInfoCircle} />
                </span>
            </button>
        </div>
    );
}

Alert.propTypes = {
    title: PropTypes.string.isRequired,
    severity: PropTypes.string.isRequired,
    time: PropTypes.number.isRequired,
    regions: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    uri: PropTypes.string.isRequired,
    expires: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired
};

export default Alert;
