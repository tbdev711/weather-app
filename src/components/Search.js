import React, { useState, useRef, useEffect } from 'react';
import Fade from 'react-reveal/Fade';
import axios from 'axios';
import { debounce } from 'lodash';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

import { Cities } from './Cities';

const REVERSE_PROXY_DESTINATION_URL = '/api/cities/';

export function Search({ onCitySelect }) {
    const [cities, setCities] = useState([]);
    const [shouldShowCities, setShouldShowCities] = useState(false);
    const searchRef = useRef();

    useEffect(() => {
        // add when mounted
        document.addEventListener('mouseup', handleClick);
        // return function to be called when unmounted
        return () => {
            document.removeEventListener('mouseup', handleClick);
        };
    }, []);

    function handleClick(e) {
        if (searchRef.current && searchRef.current.contains(e.target)) {
            return;
        }
        setShouldShowCities(false);
    }

    function handleOnCitySelect(city) {
        setShouldShowCities(false);
        onCitySelect(city);
    }

    async function fetchCities(city) {
        const response = await axios.get(REVERSE_PROXY_DESTINATION_URL + city);
        setCities(response.data);
    }

    const debouncedOnSearch = debounce(fetchCities, 500);

    return (
        <div className="hero is-navy is-fullheight">
            <div className="hero-body">
                <Fade bottom>
                    <div className="container has-text-centered">
                        <h3 className="title has-text-white has-text-left">TB-Dev Weather</h3>
                        <div className="dropdown is-active">
                            <div className="dropdown-trigger">
                                <div className="control has-icons-left has-icons-right">
                                    <input
                                        ref={searchRef}
                                        onChange={({ target: { value } }) => {
                                            value && debouncedOnSearch(value);
                                        }}
                                        onFocus={() => setShouldShowCities(true)}
                                        className="input is-large"
                                        type="text"
                                        placeholder="Search location"
                                    />
                                    <span className="icon is-left">
                                        <FontAwesomeIcon icon={faMapMarkerAlt} />
                                    </span>
                                </div>
                            </div>
                            <Cities
                                shouldShowCities={shouldShowCities}
                                cities={cities}
                                onCitySelect={handleOnCitySelect}
                            />
                        </div>
                    </div>
                </Fade>
            </div>
        </div>
    );
}

Search.propTypes = {
    onCitySelect: PropTypes.func.isRequired
};
