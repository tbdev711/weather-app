import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Map } from '../components/map';
import { Chart } from '../components/chart';
import Fade from 'react-reveal/Fade';
import { Daily } from './Daily';
import { SunMoon } from './SunMoon';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';

const datapointsPerView = 8;

export function DetailedSummary({ lat, lng, hourly = { data: [] }, daily = { data: [] } }) {
    const [dataIndex, setDataIndex] = useState(0);
    const hourlyData = hourly.data
        .slice(0, 47)
        .slice(datapointsPerView * dataIndex, datapointsPerView * (dataIndex + 1))
        .map(({ temperature, humidity, time, precipProbability }) => ({
            temperature: Math.round(temperature),
            humidity,
            time,
            precipProbability
        }));

    return (
        <>
            <Fade bottom>
                <br />
                <br />
                <br />
                <div className="title is-3 has-text-centered has-text-white has-background-primary"> Summary </div>
                <div className="tile is-parent has-text-centered">
                    <div className="tile is-child box has-background-light">
                        {daily.data.slice(0, 1).map(({ sunriseTime, sunsetTime, moonPhase, icon }) => (
                            <SunMoon
                                key={daily.time}
                                sunriseTime={sunriseTime}
                                sunsetTime={sunsetTime}
                                moonPhase={moonPhase}
                                datetimeFormat="h:mm A"
                                icon={icon}
                            />
                        ))}
                    </div>
                    <div className="tile is-child box">
                        <p className="title is-5"> 48hr Outlook</p>

                        <Chart data={hourlyData} datetimeFormat={'h A ddd'} />

                        {dataIndex > 0 && (
                            <span
                                className="media-left has-text-centered has-text-primary has-pointer hoverEffect"
                                onClick={() => setDataIndex(dataIndex - 1)}
                            >
                                <FontAwesomeIcon icon={faAngleLeft} size="3x" />
                            </span>
                        )}
                        {console.log(hourly.data.length - 1)}
                        {dataIndex * datapointsPerView < hourly.data.length - 2 && hourly.data !== 'undefined' && (
                            <span
                                className="media-left has-text-centered has-text-primary has-pointer hoverEffect"
                                onClick={() => setDataIndex(dataIndex + 1)}
                            >
                                <FontAwesomeIcon icon={faAngleRight} size="3x" />
                            </span>
                        )}
                    </div>
                </div>
                <br />
                <br />
                <div className="title is-3 has-text-centered has-text-white has-background-primary">
                    {' '}
                    Daily Forecast
                </div>
                <div className="tile is-ancestor has-text-centered">
                    <div className="tile is-parent">
                        {daily.data
                            .slice(0, 7)
                            .map(({ time, icon, apparentTemperatureHigh, apparentTemperatureLow, summary }) => (
                                <Daily
                                    key={daily.time}
                                    time={time}
                                    dateFormat="ddd"
                                    icon={icon}
                                    apparentTemperatureHigh={apparentTemperatureHigh}
                                    apparentTemperatureLow={apparentTemperatureLow}
                                    summary={summary}
                                />
                            ))}
                    </div>
                </div>
                <br /> <br />
                <div className="title is-3 has-text-centered has-text-white has-background-primary"> Radar</div>
                <div className="box cascade has-background-light">
                    <Map lat={lat} lng={lng} style={{ height: '25%', width: '50%' }} />
                </div>
            </Fade>
        </>
    );
}

DetailedSummary.propTypes = {
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
    hourly: PropTypes.array.isRequired,
    daily: PropTypes.array.isRequired
};
