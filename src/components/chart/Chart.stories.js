import React from 'react';
import moment from 'moment';

import { Chart } from '.';

export default {
    title: 'Chart',
    component: Chart
};

const data = [
    {
        time: moment().startOf('hour').valueOf(),
        temperature: 80,
        humidity: 0.9
    },
    {
        time: moment().add(1, 'hour').startOf('hour').valueOf(),
        temperature: 75,
        humidity: 0.75
    },
    {
        time: moment().add(2, 'hour').startOf('hour').valueOf(),
        temperature: 80,
        humidity: 0.86
    },
    {
        time: moment().add(3, 'hour').startOf('hour').valueOf(),
        temperature: 90,
        humidity: 0.87
    },
    {
        time: moment().add(4, 'hour').startOf('hour').valueOf(),
        temperature: 80,
        humidity: 0.5
    },
    {
        time: moment().add(5, 'hour').startOf('hour').valueOf(),
        temperature: 78,
        humidity: 0.5
    },
    {
        time: moment().add(6, 'hour').startOf('hour').valueOf(),
        temperature: 75,
        humidity: 0.25
    },
    {
        time: moment().add(7, 'hour').startOf('hour').valueOf(),
        temperature: 72,
        humidity: 0.0
    }
];

export const ChartWithHourlyData = () => (
    <div style={{ height: 300 }}>
        <Chart datetimeFormat="h:mm A" data={data} />
    </div>
);

const dailyData = [
    {
        time: moment().startOf('day').valueOf(),
        temperature: 80,
        humidity: 0.9
    },
    {
        time: moment().add(1, 'days').startOf('days').valueOf(),
        temperature: 75,
        humidity: 0.75
    },
    {
        time: moment().add(2, 'days').startOf('days').valueOf(),
        temperature: 80,
        humidity: 0.86
    },
    {
        time: moment().add(3, 'days').startOf('days').valueOf(),
        temperature: 90,
        humidity: 0.87
    },
    {
        time: moment().add(4, 'days').startOf('days').valueOf(),
        temperature: 80,
        humidity: 0.5
    },
    {
        time: moment().add(5, 'days').startOf('days').valueOf(),
        temperature: 78,
        humidity: 0.5
    },
    {
        time: moment().add(6, 'days').startOf('days').valueOf(),
        temperature: 75,
        humidity: 0.25
    },
    {
        time: moment().add(7, 'days').startOf('days').valueOf(),
        temperature: 72,
        humidity: 0.0
    }
];

export const ChartWithDailyData = () => (
    <div style={{ height: 450 }}>
        <Chart datetimeFormat="dddd" data={dailyData} />
    </div>
);
