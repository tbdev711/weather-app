import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import {
    ResponsiveContainer,
    ComposedChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Line,
    Area,
    Tooltip,
    Label,
    LabelList,
    Legend
} from 'recharts';

import './chart-styles.scss';

const defaultTickStyle = {
    fontSize: 11
};

export function Chart({
    data = [],
    datetimeFormat,
    barColor = '#318884',
    lineColor = '#413ea0',
    areaFill = '#66fcf1',
    areaStroke = '#00b7ff',
    gridStroke = '#f5f5f5',
    tickStyle = defaultTickStyle
}) {
    const transformedData = data.map(({ time, humidity, precipProbability, ...weather }) => {
        return {
            ...weather,
            humidity: Math.floor(humidity * 100),
            time: moment.unix(time).format(datetimeFormat),
            precipProbability: Math.floor(precipProbability * 100)
        };
    });

    return (
        <ResponsiveContainer width="99%" height={400}>
            <ComposedChart data={transformedData}>
                <XAxis
                    angle={0}
                    height={80}
                    tickMargin={30}
                    tick={tickStyle}
                    dataKey="time"
                    padding={{ left: 0, right: 0 }}
                />
                <YAxis
                    tickFormatter={(value) => `${value}°`}
                    domain={['dataMin - 5', 'dataMax + 5']}
                    yAxisId="1"
                    dataKey="temperature"
                >
                    <Label angle={-90} value="Temperature" position="insideLeft" style={{ textAnchor: 'middle' }} />
                </YAxis>
                <YAxis
                    tickFormatter={(value) => `${value}%`}
                    domain={[0, 100]}
                    yAxisId="2"
                    dataKey="humidity"
                    orientation="right"
                >
                    <Label
                        angle={-90}
                        value="Humidity / Precipitation"
                        position="insideRight"
                        style={{ textAnchor: 'middle' }}
                    />
                </YAxis>
                <Tooltip
                    formatter={(value, name, props) => {
                        switch (name) {
                            case 'temperature':
                                return [`${value}°`, 'Temperature'];
                            case 'humidity':
                                return [`${value}%`, 'Humidity'];
                            case 'precipProbability':
                                return [`${value}%`, 'Chance of Rain'];
                            default:
                        }
                    }}
                />
                <CartesianGrid stroke={gridStroke} />
                <Area
                    label="humidity"
                    yAxisId="2"
                    type="monotone"
                    dataKey="precipProbability"
                    fill={areaFill}
                    stroke={areaStroke}
                />
                <Line label="Humidity" yAxisId="2" type="monotone" dataKey="humidity" stroke={lineColor} />
                <Bar label="Temperature" yAxisId="1" dataKey="temperature" barSize={20} fill={barColor}>
                    <LabelList dataKey="temperature" position="top" />
                </Bar>
                <Legend />
            </ComposedChart>
        </ResponsiveContainer>
    );
}

Chart.propTypes = {
    datetimeFormat: PropTypes.string.isRequired,
    barColor: PropTypes.string,
    lineColor: PropTypes.string,
    areaFill: PropTypes.string,
    areaStroke: PropTypes.string,
    gridStroke: PropTypes.string,
    tickStyle: PropTypes.object,
    data: PropTypes.arrayOf(
        PropTypes.shape({
            time: PropTypes.number.isRequired,
            temperature: PropTypes.number.isRequired,
            humidity: PropTypes.number.isRequired,
            precipProbability: PropTypes.number.isRequired
        })
    ).isRequired
};
