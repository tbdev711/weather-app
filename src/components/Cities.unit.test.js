import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import { Cities } from './Cities';

describe('The Cities Component', () => {
    it('should appear when shouldShowCities is true and there are cities', () => {
        // arrange
        const props = {
            shouldShowCities: true,
            cities: [
                {
                    formatted_address: 'address'
                }
            ],
            onCitySelect: jest.fn()
        };

        // act
        const { getByTestId } = render(<Cities {...props} />);

        // assert
        expect(getByTestId('cities-dropdown')).toBeVisible();
    });
    it('should not appear when shouldShowCities is false', () => {
        // arrange
        const props = {
            shouldShowCities: false,
            cities: [
                {
                    formatted_address: 'address'
                }
            ],
            onCitySelect: jest.fn()
        };

        // act
        const { getByTestId } = render(<Cities {...props} />);

        // assert
        expect(getByTestId('cities-dropdown')).not.toBeVisible();
    });
    it('should not appear when there are not cities', () => {
        // arrange
        const props = {
            shouldShowCities: true,
            cities: [],
            onCitySelect: jest.fn()
        };

        // act
        const { getByTestId } = render(<Cities {...props} />);

        // assert
        expect(getByTestId('cities-dropdown')).not.toBeVisible();
    });
    it("should show the the cities' formatted address", () => {
        // arrange
        const props = {
            shouldShowCities: true,
            cities: [
                {
                    formatted_address: 'address'
                }
            ],
            onCitySelect: jest.fn()
        };

        // act
        const { getByText } = render(<Cities {...props} />);

        // assert
        expect(getByText('address')).toBeVisible();
    });
    it('should call onCitySelect with the city when the city is selected', () => {
        // arrange
        const props = {
            shouldShowCities: true,
            cities: [
                {
                    formatted_address: 'address'
                }
            ],
            onCitySelect: jest.fn()
        };

        // act
        const { getByText } = render(<Cities {...props} />);
        fireEvent.click(getByText('address'));

        // assert
        expect(props.onCitySelect).toHaveBeenCalledWith({ formatted_address: 'address' });
    });
});
