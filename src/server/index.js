require('dotenv').config();
const express = require('express');
const app = express();
const path = require('path');

// import route handlers
const routes = require('./routes');
app.use('/api', routes);

app.use(express.static(path.join(__dirname, '..', '..', 'build')));

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '..', '..', 'build', 'index.html'));
});

app.listen(process.env.PORT, () => console.log(`Weather backend listening at http://localhost:${process.env.PORT}`));
