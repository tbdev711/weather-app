const { getWeatherByLatLng } = require('./weather');
const { getCitiesByQuery, getCityByLatLng } = require('./cities');
const { getNews } = require('./news');

const { Router } = require('express');
const router = new Router();

router.get('/weather/:lat/:lng', getWeatherByLatLng);
router.get('/cities/:city', getCitiesByQuery);
router.get('/cities/:lat/:lng', getCityByLatLng);
router.get('/news', getNews);

module.exports = router;
