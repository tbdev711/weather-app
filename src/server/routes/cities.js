const axios = require('axios');

const CITIES_LOCATION = `https://maps.googleapis.com/maps/api/geocode/json?key=${process.env.API_KEY}&address=CITY_LOCATION`;
const AUTO_LOCATION = `https://maps.googleapis.com/maps/api/geocode/json?latlng=LAT,LNG&key=${process.env.API_KEY}`;

async function getCitiesByQuery(request, response) {
    try {
        const { city } = request.params;

        const { data } = await axios.get(CITIES_LOCATION.replace('CITY_LOCATION', city));

        response.json(data.results);
    } catch (error) {
        response.status(500).end();
    }
}

async function getCityByLatLng(request, response) {
    try {
        const { lat, lng } = request.params;

        const { data } = await axios.get(AUTO_LOCATION.replace('LAT', lat).replace('LNG', lng));

        response.json(data.results);
    } catch (error) {
        response.status(500).end();
    }
}

module.exports = {
    getCitiesByQuery,
    getCityByLatLng
};
