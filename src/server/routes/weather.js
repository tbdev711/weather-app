const axios = require('axios');

const WEATHER_TEMPLATE = `https://api.darksky.net/forecast/${process.env.KEY}/LAT,LNG`;

async function getWeatherByLatLng(request, response) {
    try {
        const { data } = await axios.get(
            WEATHER_TEMPLATE.replace('LAT', request.params.lat).replace('LNG', request.params.lng)
        );

        response.json(data);
    } catch (error) {
        // send an unspecified server error code and end the request
        response.status(500).end();
    }
}

module.exports = {
    getWeatherByLatLng
};
