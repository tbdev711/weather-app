import axios from 'axios';

import { getCitiesByQuery, getCityByLatLng } from './cities';

jest.mock('axios');

describe('getCitiesByQuery route', () => {
    beforeEach(jest.resetAllMocks);

    it('should return the results as a JSON object', async () => {
        // arrange
        const request = {
            params: {}
        };
        const response = {
            json: jest.fn()
        };
        axios.get.mockResolvedValue({ data: 'test data' });

        // act
        await getCitiesByQuery(request, response);

        // assert
        expect(axios.get).toHaveBeenCalledWith(expect.any(String));
        expect(response.json).toHaveBeenCalled();
    });

    it('should handle errors with a 500 status', async () => {
        // arrange
        const end = jest.fn();
        const request = {};
        const response = {
            status: jest.fn().mockReturnValue({ end })
        };
        axios.get.mockRejectedValue();

        // act
        await getCitiesByQuery(request, response);

        // assert
        expect(response.status).toHaveBeenCalledWith(500);
        expect(end).toHaveBeenCalled();
    });
});

describe('getCityByLatLng route', () => {
    it('should return the results as a JSON object', async () => {
        // arrange
        const request = {
            params: {}
        };
        const response = {
            json: jest.fn()
        };
        axios.get.mockResolvedValue({ data: 'test data' });

        // act
        await getCityByLatLng(request, response);

        // assert
        expect(axios.get).toHaveBeenCalledWith(expect.any(String));
        expect(response.json).toHaveBeenCalled();
    });

    it('should handle errors with a 500 status', async () => {
        // arrange
        const end = jest.fn();
        const request = {};
        const response = {
            status: jest.fn().mockReturnValue({ end })
        };
        axios.get.mockRejectedValue();

        // act
        await getCityByLatLng(request, response);

        // assert
        expect(response.status).toHaveBeenCalledWith(500);
        expect(end).toHaveBeenCalled();
    });
});
