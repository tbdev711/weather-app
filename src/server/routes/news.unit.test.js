import axios from 'axios';

import { getNews } from './news';

jest.mock('axios');

describe('getNews route', () => {
    beforeEach(jest.resetAllMocks);

    it('should make a request and return the results as a JSON object', async () => {
        // arrange
        const request = {};
        const response = {
            json: jest.fn()
        };
        axios.get.mockResolvedValue({ data: 'test data' });

        // act
        await getNews(request, response);

        // assert
        expect(axios.get).toHaveBeenCalledWith(expect.any(String));
        expect(response.json).toHaveBeenCalledWith('test data');
    });

    it('should handle errors with a 500 status', async () => {
        // arrange
        const end = jest.fn();
        const request = {};
        const response = {
            status: jest.fn().mockReturnValue({ end })
        };
        axios.get.mockRejectedValue();

        // act
        await getNews(request, response);

        // assert
        expect(response.status).toHaveBeenCalledWith(500);
        expect(end).toHaveBeenCalled();
    });
});
