import express from 'express';
import axios from 'axios';
import request from 'supertest';

import router from '.';

jest.mock('axios');

const app = express();
app.use('/', router);

describe('The Server Routes', () => {
    it('should allow requests to /weather/:lat/:lng', (done) => {
        // arrange
        axios.get.mockResolvedValue({});

        // act
        // assert
        request(app).get('/weather/lat/lng').expect(200, done);
    });
    it('should allow requests to /cities/:city', (done) => {
        // arrange
        axios.get.mockResolvedValue({ data: {} });

        // act
        // assert
        request(app).get('/cities/city').expect(200, done);
    });
    it('should allow requests to /cities/:lat/:lng', (done) => {
        // arrange
        axios.get.mockResolvedValue({ data: {} });

        // act
        // assert
        request(app).get('/cities/lat/lng').expect(200, done);
    });
    it('should allow requests to /news', (done) => {
        // arrange
        axios.get.mockResolvedValue({});

        // act
        // assert
        request(app).get('/news').expect(200, done);
    });
});
