const axios = require('axios');

const NEWS_URL = `https://newsapi.org/v2/everything?domains=weather.com&apiKey=${process.env.API_NEWS_KEY}`;

async function getNews(request, response) {
    try {
        const { data } = await axios.get(NEWS_URL);

        response.json(data);
    } catch (error) {
        // send an unspecified server error code and end the request
        response.status(500).end();
    }
}

module.exports = {
    getNews
};
