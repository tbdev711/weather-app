import { getWeatherByLatLng } from './weather';

import axios from 'axios';

jest.mock('axios');

describe('getWeatherByLatLng route', () => {
    beforeEach(jest.resetAllMocks);

    it('should return the results as a JSON object', async () => {
        // arrange
        const request = {
            params: {}
        };
        const response = {
            json: jest.fn()
        };
        axios.get.mockResolvedValue({ data: 'test data' });

        // act
        await getWeatherByLatLng(request, response);

        // assert
        expect(axios.get).toHaveBeenCalledWith(expect.any(String));
        expect(response.json).toHaveBeenCalledWith('test data');
    });

    it('should handle errors with a 500 status', async () => {
        // arrange
        const end = jest.fn();
        const request = {};
        const response = {
            status: jest.fn().mockReturnValue({ end })
        };
        axios.get.mockRejectedValue();

        // act
        await getWeatherByLatLng(request, response);

        // assert
        expect(response.status).toHaveBeenCalledWith(500);
        expect(end).toHaveBeenCalled();
    });
});
