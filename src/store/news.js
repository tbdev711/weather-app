import axios from 'axios';
import { catchError } from './error';
import { createSlice } from '@reduxjs/toolkit';

const REVERSE_PROXY_NEWS_URL = '/api/news/';

const newsSlice = createSlice({
    name: 'news',
    initialState: [],
    reducers: {
        fetchNews: (state, action) => action.payload
    }
});

export function fetchNews() {
    return async function (dispatch) {
        try {
            const response = await axios.get(REVERSE_PROXY_NEWS_URL);
            dispatch(newsSlice.actions.fetchNews(response.data.articles));
        } catch (error) {
            dispatch(catchError(error.message));
        }
    };
}

export default newsSlice.reducer;
