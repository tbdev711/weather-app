import axios from 'axios';

import cityName, { selectCityNameByLocation, selectCity } from './city-name';
import { catchError } from './error';

jest.mock('axios');

describe('The City Name Slice', () => {
    it('should select the city name', () => {
        // arrange
        const initialState = '';
        const expectedResult = '123';

        // act
        const actualResult = cityName(initialState, { type: 'city-name/selectCity', payload: expectedResult });

        // assert
        expect(actualResult).toEqual(expectedResult);
    });

    describe('selectCityNameByLocation', () => {
        beforeEach(axios.get.mockReset);

        it('should not select city name by location when response is undefined', async () => {
            // arrange
            axios.get.mockResolvedValue({});
            const dispatch = jest.fn();

            // act
            await selectCityNameByLocation({})(dispatch);

            expect(dispatch).not.toHaveBeenCalled();
        });

        it('should not select city name by location when there are no cities in the response', async () => {
            // arrange
            axios.get.mockResolvedValue({ data: [] });
            const dispatch = jest.fn();

            // act
            await selectCityNameByLocation({})(dispatch);

            expect(dispatch).not.toHaveBeenCalled();
        });

        it('should select city name when locality and administrative area are defined', async () => {
            // arrange
            const address_components = [
                {
                    long_name: 'long_name',
                    types: ['locality']
                },
                {
                    short_name: 'short_name',
                    types: ['administrative_area_level_1']
                }
            ];
            axios.get.mockResolvedValue({ data: [{ address_components }] });
            const dispatch = jest.fn();

            // act
            await selectCityNameByLocation({})(dispatch);

            // assert
            expect(dispatch).toHaveBeenCalledWith(selectCity('long_name, short_name'));
        });

        it('should select city name when locality is defined but administrative area is not', async () => {
            // arrange
            const address_components = [
                {
                    long_name: 'long_name',
                    types: ['locality']
                },
                {
                    short_name: 'short_name',
                    types: ['']
                }
            ];
            axios.get.mockResolvedValue({ data: [{ address_components }] });
            const dispatch = jest.fn();

            // act
            await selectCityNameByLocation({})(dispatch);

            // assert
            expect(dispatch).toHaveBeenCalledWith(selectCity('long_name, '));
        });

        it('should select city name when there are no formatted address', async () => {
            // arrange
            const address_components = undefined;
            axios.get.mockResolvedValue({ data: [{ address_components }] });
            const dispatch = jest.fn();

            // act
            await selectCityNameByLocation({})(dispatch);

            // assert
            expect(dispatch).toHaveBeenCalledWith(selectCity(', '));
        });

        it('should select city name when administrative area is defined but locality is not', async () => {
            // arrange
            const address_components = [
                {
                    long_name: 'long_name',
                    types: ['']
                },
                {
                    short_name: 'short_name',
                    types: ['administrative_area_level_1']
                }
            ];
            axios.get.mockResolvedValue({ data: [{ address_components }] });
            const dispatch = jest.fn();

            // act
            await selectCityNameByLocation({})(dispatch);

            // assert
            expect(dispatch).toHaveBeenCalledWith(selectCity(', short_name'));
        });

        it('should catch error', async () => {
            // arrange
            axios.get.mockRejectedValue({ message: 'message' });
            const dispatch = jest.fn();

            // act
            await selectCityNameByLocation({})(dispatch);

            expect(dispatch).toHaveBeenCalledWith(catchError('message'));
        });
    });
});
