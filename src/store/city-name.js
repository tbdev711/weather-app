import { catchError } from './error';
import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';

const REVERSE_PROXY_DESTINATION_URL = '/api/cities/';

const cityNameSlice = createSlice({
    name: 'city-name',
    initialState: '',
    reducers: {
        selectCity: (state, action) => action.payload
    }
});

export const { selectCity } = cityNameSlice.actions;

export function selectCityNameByLocation(location) {
    return async function (dispatch) {
        try {
            const response = await axios.get(`${REVERSE_PROXY_DESTINATION_URL}${location.lat}/${location.lng}`);

            // obtain the first cities name from the response
            if (response.data && response.data.length > 0) {
                const [{ address_components = [] }] = response.data;

                const city =
                    (address_components.find((component) => component.types.includes('locality')) || {}).long_name ||
                    '';
                const stateName =
                    (
                        address_components.find((component) =>
                            component.types.includes('administrative_area_level_1')
                        ) || {}
                    ).short_name || '';
                const cityStateName = city.concat(', ' + stateName);
                dispatch(selectCity(cityStateName));
            }
        } catch (error) {
            dispatch(catchError(error.message));
        }
    };
}

export default cityNameSlice.reducer;
