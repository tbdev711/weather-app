import axios from 'axios';

import weather, { clearWeather, fetchCurrentWeather, fetchWeather } from './weather';
import { catchError } from './error';

jest.mock('axios');

describe('The Weather Slice', () => {
    it('fetch the weather', () => {
        // arrange
        const initialState = {};
        const expectedResult = '321';

        // act
        const actualResult = weather(initialState, { type: 'weather/fetchCurrentWeather', payload: expectedResult });

        // assert
        expect(actualResult).toEqual(expectedResult);
    });

    describe('fetchWeather', () => {
        beforeEach(axios.get.mockReset);

        it('should fetch weather', async () => {
            // arrange
            axios.get.mockResolvedValue({ data: 'data' });
            const city = {};
            const dispatch = jest.fn();

            // act
            await fetchWeather(city)(dispatch);

            // assert
            expect(dispatch).toHaveBeenCalledWith(clearWeather());
            expect(dispatch).toHaveBeenCalledWith(fetchCurrentWeather('data'));
        });

        it('should catch error', async () => {
            // arrange
            axios.get.mockRejectedValue({ message: 'message' });
            const city = {};
            const dispatch = jest.fn();

            // act
            await fetchWeather(city)(dispatch);

            // assert
            expect(dispatch).toHaveBeenCalledWith(catchError('message'));
        });
    });
});
