import { combineReducers, configureStore } from '@reduxjs/toolkit';

import news from './news';
import weather from './weather';
import cityName from './city-name';
import error from './error';
import ui from './ui';

const reducer = combineReducers({
    weather,
    news,
    cityName,
    error,
    ui
});

export const store = configureStore({
    reducer,
    devTools: process.env.NODE_ENV !== 'production'
});
