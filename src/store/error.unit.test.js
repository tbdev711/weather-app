import error from './error';

describe('The Error Slice', () => {
    it('catch errors', () => {
        // arrange
        const initialState = null;
        const expectedResult = 'XYZ';

        // act
        const actualResult = error(initialState, { type: 'error/catchError', payload: expectedResult });

        // assert
        expect(actualResult).toEqual(expectedResult);
    });
});
