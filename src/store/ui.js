import { createSlice } from '@reduxjs/toolkit';

const uiSlice = createSlice({
    name: 'ui',
    initialState: {
        isFetchingWeather: false
    },
    reducers: {
        startFetchingWeather: (state) => {
            state.isFetchingWeather = true;
        },
        stopFetchingWeather: (state) => {
            state.isFetchingWeather = false;
        }
    }
});

export const { startFetchingWeather, stopFetchingWeather } = uiSlice.actions;

export default uiSlice.reducer;
