import { createSlice } from '@reduxjs/toolkit';

const errorSlice = createSlice({
    name: 'error',
    initialState: null,
    reducers: {
        catchError: (state, action) => action.payload
    }
});

export const { catchError } = errorSlice.actions;

export default errorSlice.reducer;
