import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';

import { catchError } from './error';

const REVERSE_PROXY_WEATHER_URL = '/api/weather/';

const weatherSlice = createSlice({
    name: 'weather',
    initialState: {},
    reducers: {
        fetchCurrentWeather: (state, action) => action.payload,
        clearWeather: () => {}
    }
});

export const { fetchCurrentWeather, clearWeather } = weatherSlice.actions;

export function fetchWeather(city) {
    return async function (dispatch) {
        try {
            dispatch(clearWeather());
            const response = await axios.get(`${REVERSE_PROXY_WEATHER_URL}${city.lat}/${city.lng}`);
            dispatch(fetchCurrentWeather(response.data));
        } catch (error) {
            dispatch(catchError(error.message));
        }
    };
}

export default weatherSlice.reducer;
