import news from './news';

describe('The News Slice', () => {
    it('fetch the news', () => {
        // arrange
        const initialState = [];
        const expectedResult = 'ABC';

        // act
        const actualResult = news(initialState, { type: 'news/fetchNews', payload: expectedResult });

        // assert
        expect(actualResult).toEqual(expectedResult);
    });
});
