import PropTypes from 'prop-types';

export default {
    apparentTemperature: PropTypes.number,
    cloudCover: PropTypes.number.isRequired,
    dewPoint: PropTypes.number.isRequired,
    humidity: PropTypes.number.isRequired,
    icon: PropTypes.string,
    ozone: PropTypes.number,
    precipProbability: PropTypes.number,
    pressure: PropTypes.number.isRequired,
    summary: PropTypes.string,
    temperature: PropTypes.number,
    time: PropTypes.number,
    uvIndex: PropTypes.number.isRequired,
    visibility: PropTypes.number.isRequired,
    windSpeed: PropTypes.number,
    windGust: PropTypes.number
};
