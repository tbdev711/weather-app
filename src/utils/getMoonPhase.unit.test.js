import { getMoonPhase } from './getMoonPhase';

describe('getMoonPhase', () => {
    it('should show First Quarter when phase is 0.25', () => {
        // arrange
        const phase = 0.25;
        const expectedResult = 'First Quarter';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Full Moon when phase is 0.5', () => {
        // arrange
        const phase = 0.5;
        const expectedResult = 'Full Moon';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Third Quarter when phase is 0.75', () => {
        // arrange
        const phase = 0.75;
        const expectedResult = 'Third Quarter';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show New Moon when phase is 0', () => {
        // arrange
        const phase = 0;
        const expectedResult = 'New Moon';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Waxing Crescent - Young when phase is between 0 and 0.05', () => {
        // arrange
        const phase = 0.05;
        const expectedResult = 'Waxing Crescent - Young';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Waxing Crescent when phase is between 0.05 and 0.25', () => {
        // arrange
        const phase = 0.06;
        const expectedResult = 'Waxing Crescent';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Waxing Gibbous when phase is between 0.25 and 0.5', () => {
        // arrange
        const phase = 0.3;
        const expectedResult = 'Waxing Gibbous';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Waning Gibbous when phase is between 0.5 and 0.75', () => {
        // arrange
        const phase = 0.6;
        const expectedResult = 'Waning Gibbous';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Waning Crescent when phase is between 0.75 and 0.95', () => {
        // arrange
        const phase = 0.8;
        const expectedResult = 'Waning Crescent';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Waning Crescent - Old when phase  is between 0.95 and 1', () => {
        // arrange
        const phase = 0.96;
        const expectedResult = 'Waning Crescent - Old';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });

    it('should show Moon by default', () => {
        // arrange
        const phase = -1;
        const expectedResult = 'Moon';

        // act
        const actualResult = getMoonPhase(phase);

        // assert
        expect(actualResult.label).toEqual(expectedResult);
    });
});
