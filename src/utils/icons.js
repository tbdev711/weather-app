import { faTemperatureLow } from '@fortawesome/free-solid-svg-icons';

import cloudy from '../icons/weather-many-clouds.png';
import clearDay from '../icons/weather-clear.png';
import clearNight from '../icons/weather-clear-night.png';
import rain from '../icons/weather-showers.png';
import snow from '../icons/weather-snow.png';
import sleet from '../icons/weather-snow-rain.png';
import fog from '../icons/weather-mist.png';
import partlyCloudyDay from '../icons/weather-few-clouds.png';
import partlyCloudyNight from '../icons/weather-few-clouds-night.png';

export function getIcon(icon) {
    switch (icon) {
        case 'clear-day':
            return clearDay;
        case 'clear-night':
            return clearNight;
        case 'rain':
            return rain;
        case 'snow':
            return snow;
        case 'sleet':
            return sleet;
        case 'fog':
            return fog;
        case 'cloudy':
            return cloudy;
        case 'partly-cloudy-day':
            return partlyCloudyDay;
        case 'partly-cloudy-night':
            return partlyCloudyNight;

        default:
            return faTemperatureLow;
    }
}
