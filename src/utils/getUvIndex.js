export function getUvIndex(uvIndex) {
    if (uvIndex < 3) {
        return 'Low';
    } else if (uvIndex < 6) {
        return 'Moderate';
    } else if (uvIndex < 8) {
        return 'High';
    } else if (uvIndex <= 10) {
        return 'Very High';
    }

    return 'Extreme';
}
