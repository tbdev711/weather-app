import { getIcon } from './icons';

describe('getIcon', () => {
    it('returns the default icon', () => {
        // arrange
        // act
        const icon = getIcon();

        // assert
        expect(icon.iconName).toEqual('temperature-low');
    });
});
