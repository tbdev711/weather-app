import { ReactComponent as FirstQuarterIcon } from '../svg/First-Quarter.svg';
import { ReactComponent as FullMoonIcon } from '../svg/Full-Moon.svg';
import { ReactComponent as ThirdQuarterIcon } from '../svg/Third-Quarter.svg';
import { ReactComponent as NewMoonIcon } from '../svg/New-Moon.svg';
import { ReactComponent as WaxingCrescentYoungIcon } from '../svg/Waxing-Crescent-Young.svg';
import { ReactComponent as WaxingCrescentIcon } from '../svg/Waxing-Crescent.svg';
import { ReactComponent as WaxingGibbousIcon } from '../svg/Waxing-Gibbous.svg';
import { ReactComponent as WaningGibbousIcon } from '../svg/Waning-Gibbous.svg';
import { ReactComponent as WaningCrescentIcon } from '../svg/Waxing-Crescent.svg';
import { ReactComponent as WaningCrescentOldIcon } from '../svg/Waning-Crescent-Old.svg';
import { ReactComponent as DefaultIcon } from '../svg/New-Moon.svg';

export function getMoonPhase(phase) {
    if (phase === 0.25) {
        return {
            Icon: FirstQuarterIcon,
            label: 'First Quarter'
        };
    } else if (phase === 0.5) {
        return {
            Icon: FullMoonIcon,
            label: 'Full Moon'
        };
    } else if (phase === 0.75) {
        return {
            Icon: ThirdQuarterIcon,
            label: 'Third Quarter'
        };
    } else if (phase === 0) {
        return {
            Icon: NewMoonIcon,
            label: 'New Moon'
        };
    } else if (phase > 0 && phase <= 0.05) {
        return {
            Icon: WaxingCrescentYoungIcon,
            label: 'Waxing Crescent - Young'
        };
    } else if (phase > 0.05 && phase < 0.25) {
        return {
            Icon: WaxingCrescentIcon,
            label: 'Waxing Crescent'
        };
    } else if (phase > 0.25 && phase < 0.5) {
        return {
            Icon: WaxingGibbousIcon,
            label: 'Waxing Gibbous'
        };
    } else if (phase > 0.5 && phase < 0.75) {
        return {
            Icon: WaningGibbousIcon,
            label: 'Waning Gibbous'
        };
    } else if (phase > 0.75 && phase < 0.95) {
        return {
            Icon: WaningCrescentIcon,
            label: 'Waning Crescent'
        };
    } else if (phase >= 0.95 && phase < 1) {
        return {
            Icon: WaningCrescentOldIcon,
            label: 'Waning Crescent - Old'
        };
    }
    return {
        Icon: DefaultIcon,
        label: 'Moon'
    };
}
