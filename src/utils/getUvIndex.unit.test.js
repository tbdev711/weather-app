import { getUvIndex } from './getUvIndex';

describe('getUvIndex', () => {
    it('should return "Low" when uvIndex is less than 3', () => {
        // arrange
        const uvIndex = 2;
        const expectedValue = 'Low';

        // act
        const actualValue = getUvIndex(uvIndex);

        // assert
        expect(actualValue).toEqual(expectedValue);
    });

    it('should return "Moderate" when uvIndex is greater than 3 but less than 6', () => {
        // arrange
        const uvIndex = 4;
        const expectedValue = 'Moderate';

        // act
        const actualValue = getUvIndex(uvIndex);

        // assert
        expect(actualValue).toEqual(expectedValue);
    });

    it('should return "High" when uvIndex is greater than 6 but less than 8', () => {
        // arrange
        const uvIndex = 7;
        const expectedValue = 'High';

        // act
        const actualValue = getUvIndex(uvIndex);

        // assert
        expect(actualValue).toEqual(expectedValue);
    });

    it('should return "Very High" when uvIndex is greater than 8 but less than or equal to 10', () => {
        // arrange
        const uvIndex = 9;
        const expectedValue = 'Very High';

        // act
        const actualValue = getUvIndex(uvIndex);

        // assert
        expect(actualValue).toEqual(expectedValue);
    });

    it('should return "Extreme" when uvIndex is greater than 10', () => {
        // arrange
        const uvIndex = 11;
        const expectedValue = 'Extreme';

        // act
        const actualValue = getUvIndex(uvIndex);

        // assert
        expect(actualValue).toEqual(expectedValue);
    });
});
