const degrees = ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];

export function getWindDirection(degree) {
    const index = parseInt(degree / 22.5 + 0.5) % 16;
    return degrees[index];
}
