import { useEffect, useState } from 'react';

export function useGeolocation(callback) {
    const [hasRequestedLocation, setHasRequestedLocation] = useState(false);

    useEffect(() => {
        if (navigator.geolocation && !hasRequestedLocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                const lng = position.coords.longitude;
                const lat = position.coords.latitude;
                callback({ lat, lng });
            });
            setHasRequestedLocation(true);
        }
    }, [hasRequestedLocation, callback]);
}
