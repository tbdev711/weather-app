import React from 'react';
import { useGeolocation } from './use-geolocation';
import { act, render } from '@testing-library/react';

describe('The useGeolocation Hook', () => {
    beforeEach(navigator.geolocation.getCurrentPosition.mockReset);

    it('should call callback with lat and lng', () => {
        // arrange
        const DummyComponent = ({ callback }) => {
            useGeolocation(callback);
            return null;
        };
        navigator.geolocation.getCurrentPosition.mockImplementation((callback) => {
            callback({ coords: { latitude: 'latitude', longitude: 'longitude' } });
        });
        const testCallback = jest.fn();

        // act
        render(<DummyComponent callback={testCallback} />);

        // assert
        expect(testCallback).toHaveBeenCalledWith({ lat: 'latitude', lng: 'longitude' });
    });

    it('should only request geolocation once', () => {
        // arrange
        const DummyComponent = ({ callback }) => {
            useGeolocation(callback);
            return null;
        };
        navigator.geolocation.getCurrentPosition.mockImplementation((callback) => {
            callback({ coords: { latitude: 'latitude', longitude: 'longitude' } });
        });
        const testCallback = jest.fn();

        // act
        const { rerender } = render(<DummyComponent callback={testCallback} />);

        act(() => {
            rerender();
        });

        // assert
        expect(testCallback).toHaveBeenCalledTimes(1);
    });
});
