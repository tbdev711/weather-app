import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectCityNameByLocation } from '../store/city-name';
import { useGeolocation } from '../hooks';
import { fetchWeather } from '../store/weather';
import { Home } from '../pages';

export function Weather() {
    const dispatch = useDispatch();
    const cityName = useSelector((reduxState) => reduxState.cityName);
    const cityState = useSelector((reduxState) => reduxState.cityState);
    const weather = useSelector((state) => {
        return state.weather;
    });
    const [{ lat, lng }, setLatLng] = useState({ lat: 82, lng: 42 });
    useGeolocation(handleGetWeather);

    function handleGetWeather({ lat, lng }) {
        dispatch(selectCityNameByLocation({ lat, lng }));
        dispatch(fetchWeather({ lat, lng }));
        setLatLng({ lat, lng });
    }

    function onCitySelect({
        geometry: {
            location: { lat, lng }
        }
    }) {
        handleGetWeather({ lat, lng });
    }

    return (
        <Home
            weather={weather}
            onCitySelect={onCitySelect}
            cityState={cityState}
            cityName={cityName}
            geolocation={{ lat, lng }}
        />
    );
}
