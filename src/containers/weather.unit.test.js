import React from 'react';
import { Weather } from './weather';
import { Home } from '../pages';
import { render, act, waitForElement } from '@testing-library/react';
import { useDispatch } from 'react-redux';

jest.mock('../pages');
jest.mock('react-redux');

describe('The Weather Container', () => {
    let onCitySelect;
    const dispatch = jest.fn();

    beforeAll(() => {
        useDispatch.mockReturnValue(dispatch);
    });

    beforeEach(() => {
        Home.mockImplementation((props) => {
            onCitySelect = props.onCitySelect;
            return (
                <p>
                    {props.geolocation.lat}
                    {props.geolocation.lng}
                </p>
            );
        });
        dispatch.mockReset();
    });

    it('should get the weather and update the geolocation', async () => {
        // arrange
        const city = {
            geometry: {
                location: {
                    lat: 'lat',
                    lng: 'lng'
                }
            }
        };

        // act
        const { queryByText, getByText } = render(<Weather />);
        act(() => {
            onCitySelect(city);
        });

        await waitForElement(() => getByText('latlng'));

        // assert
        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(queryByText('latlng')).toBeInTheDocument();
    });
});
