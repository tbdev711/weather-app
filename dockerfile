FROM node:latest

WORKDIR /urs/src/app

COPY package*.json ./

# Install production dependencies.
RUN npm install --only=production

# Copy local code to the container image.
COPY . ./

RUN npm run build

CMD ["node", "src/server"]
